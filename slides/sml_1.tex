\documentclass[h]{beamer}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{wrapfig}
\usepackage{amsfonts}
\usepackage{csquotes}
\usepackage{epigraph}
\usepackage{float}
\usepackage{lipsum}
\usepackage{blindtext}
\usepackage{multirow}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{bm}
\usepackage{xcolor}
\usepackage{upgreek}
\usepackage{calc}
\usepackage{lscape}
\usepackage{graphicx}
\usepackage{xcolor}





\def\leq{\leqslant}
\def\geq{\geqslant}

\definecolor{MyBackground}{RGB}{248,248,248}
\usetheme{Copenhagen}

\definecolor{darkblue}{rgb}{0,0,0.8}
\definecolor{darkGrey}{rgb}{0.0,0.3,0.35}

\usecolortheme[rgb={0.0,0.4,0.4}]{structure}
\definecolor{mygreen}{rgb}{0,0.4,0.4}
\setbeamercolor{background canvas}{bg=MyBackground}
%\setbeamercolor*{palette quaternary}{fg=MyBackground,bg=darkGrey}

%\definecolor{MyBackground}{rgb}{1.0000,0.9451,0.6549}
%-----------------------------------------------%
%-----------------------------------------------%



%
% TITLE SLIDE
%

\title{Adversarial attacks: an application to traffic signs images}

\author[L. Basile, R. Corti, G. Rossato]{Lorenzo Basile, Roberto Corti, Giacomo Rossato}

\date{June 29, 2020}
\institute{Statistical Machine Learning exam}

\titlegraphic{\includegraphics[width=4.2cm,keepaspectratio]{images/logo-universita.png}
	\hspace*{13.04mm}~
	\includegraphics[width=3.5cm,keepaspectratio]{images/logo-dssc.png}
}

%-----------------------------------------------%
%-----------------------------------------------%



%
% ADAPTIVE TABLE OF CONTENTS
%

\AtBeginSection[]
{
	\begin{frame}
		\frametitle{Outline}
		\tableofcontents[currentsection]
	\end{frame}
}

%-----------------------------------------------%
%-----------------------------------------------%



%
% DOCUMENT
%

\begin{document}

% Insert title slide
\frame{\titlepage}

%-----------------------------------------------%



\section{The problem: adversarial attacks}
	
  %% SLIDE 1:
  \begin{frame}
	\frametitle{Deep Neural Networks: a very accurate tool}
	%-----------------------------------------%
	
		Deep neural networks (DNN) have have become increasingly popular and successful in many machine learning tasks.

		\begin{columns}
			\column{0.3\textwidth}
			\begin{figure}
				\includegraphics[scale=0.21]{images/dog.png}
				\caption*{Image recognition}
			\end{figure}
			
			
			
			\column{0.3\textwidth}
			\begin{figure}
				\includegraphics[scale=0.26]{images/voice.png}
				\caption*{\\Voice recognition}
			\end{figure}
		
			\column{0.3\textwidth}
			\begin{figure}
				\includegraphics[scale=0.09]{images/segmentation.png}
				\caption*{\\Segmentation}
			\end{figure}
		
		\end{columns}
		
	%-----------------------------------------%
  \end{frame}


  %% SLIDE 2:
  \begin{frame}[t]
	\frametitle{But...}
	%-----------------------------------------%
	 
	 
	 \onslide<1->{In recent years, many works (Szegedy et al., 2013; Goodfellow et al., 2014; He et al., 2016) have shown that DNN models are vulnerable to \textbf{adversarial examples}.  \\~~\\
 	}
	 
	 \onslide<2->{\textit{"Adversarial examples are inputs to machine learning models that an attacker intentionally designed to cause the model to make mistakes."}
 		

	 	\begin{figure}
	 		\includegraphics[scale=0.24]{images/pig.png}
	 	\end{figure}
 	}

	%-----------------------------------------%
  \end{frame}

%% SLIDE 3:
\begin{frame}[t]
\frametitle{Adversarial attacks}
	%-----------------------------------------%
	\onslide<1->
	{
		\textbf{Victim sample}: $(x,y)$ 	\\ 
		\textbf{Attacker's goal}: find $x'$ such that $f(x')=y' \ne y$  \\~~\\
	}
	\onslide<2->{
		 What is the purpose of the attacker?  Misguide on one sample or influence the overall performance?
		\onslide<3->{ 
		\begin{itemize}
			\item Targeted attack
			\item Untargeted attack
		\end{itemize}
		}
	}
	\onslide<4->{
		What information is available to the attacker?
		\onslide<5->{ 
		\begin{itemize}
			\item White-Box attack
			\item Black-Box attack
		\end{itemize}
		}
	
	}
	%-----------------------------------------%
\end{frame}

%% SLIDE 4:
\begin{frame}[t]
\frametitle{White-Box Attacks: Fast Gradient Sign Method (FGSM)}
%-----------------------------------------%
\onslide<1-> {In (Goodfellow et al., 2014), a one-step method is introduced to fast generate adversarial examples.\\~~\\}

\onslide<2->{ $$ x ' = x + \epsilon \text{sign}(\nabla_x \mathcal{L}(\theta, x, y)) ~~ \text{untargeted}$$}

\onslide<3-> {$$ x ' = x - \epsilon \text{sign}(\nabla_x \mathcal{L}(\theta, x, t)) ~~~ \text{targeted on} ~ t$$}


%-----------------------------------------%
\end{frame}

%% SLIDE 5:
\begin{frame}[t]
\frametitle{White-Box Attacks: Projected Gradient Descent (PGD)}
\onslide<1-> {In a non-targeted setting, an iterative version of the one-step FGSM attack.  Presented in Madry et al., 2017. \\~~\\ }

\onslide<2-> {Starting from a random initialization of $x^{(0)}$, $$ x^{(t+1)}=\text{Clip}_{x, \epsilon}(x^{(t)} + \alpha \text{sign} (\nabla_x \mathcal{L}(\theta, x, y)) $$ }

\onslide<3-> {PGD heuristically looks for the "most adversarial" example in the $l_{\infty}$ ball of radius $\epsilon$ around $x$.}


\end{frame}

%% SLIDE 6:
\begin{frame}[t]
\frametitle{Black-Box Attacks: Single-Pixel Attack }
\onslide<1-> {Constraining the $l_0$ "norm" of the perturbation $x'-x$ limits the number of pixels that are allowed to be changed. }

\onslide<2->{
	\begin{figure}
		\includegraphics[scale=0.4]{images/single_pixel.png}
		\caption*{(Su et al., 2019)}
	\end{figure}
	}


\end{frame}

%% SLIDE 7:
\begin{frame}[t]
\frametitle{Countermeasures Against Adversarial Examples}
	%-----------------------------------------%
	
	\begin{enumerate}
		\onslide<1->{\item Gradient Masking/Obfuscation: masking or hiding the gradients.}
		\onslide<2->{\item Robust Optimization: re-learning DNN classifier’s parameters.}
		\onslide<3->{\item Adversarial Examples Detection: learning the distribution of natural/benign examples.}
		\onslide<4->{\item Filtering: preprocessing based defenses with different kinds of spatial smoothing filters.}
	\end{enumerate}
	%-----------------------------------------%
\end{frame}

\section{Our problem: attacks on traffic signs}
%% SLIDE 8:
\begin{frame}
\frametitle{A very dangerous situation}
	%-----------------------------------------%
	\onslide<1->
	{
		In autonomous vehicles, deep convolutional neural networks are used to recognize road signs.
	}
	%-----------------------------------------%
	\onslide<2->
	{
	\begin{figure}
		\includegraphics[scale=0.24]{images/stop.png}
		\caption*{(Eykholt et al., 2017)}
	\end{figure}
	What if the classifier fails to recognize the “STOP” sign and the vehicle keeps going?
	}
\end{frame}

%% SLIDE 9:
\begin{frame}[t]
\frametitle{BelgiumTSC Dataset}
	%-----------------------------------------%
	\begin{columns}[t]
		
	\column{0.4\textwidth}
	{BelgiumTSC dataset is built for traffic sign classification purposes. 
	\\~\\
	Cropped images around annotations for 62 different classes of traffic signs. 
	 \\~\\
	4591 training images and a test set with 2534 images.
	}
	\column{0.5\textwidth}{
	\begin{figure}
		\includegraphics[scale=0.16]{images/bst.png}
	\end{figure}
	}
	\end{columns}
	%-----------------------------------------%
\end{frame}

%% SLIDE 10:
\begin{frame}
\frametitle{GoogLeNet}
	%-----------------------------------------%
	\onslide<1->{
	Moderately deep (22 layers) CNN architecture presented in 2014 by Szegedy et al. for ImageNet classification.}
	\onslide<2->{
	\\Main innovative features:
	\begin{itemize}
  	\item 1x1 convolutions for dimensionality reduction}
	\onslide<3->{
	\item Inception modules
	\end{itemize}
	\begin{center}
	\begin{figure}
		\includegraphics[width=5cm,keepaspectratio]{images/inceptionmodule.png}
		\caption*{(Szegedy et al., 2014)}
	\end{figure}
	\end{center}}
	%-----------------------------------------%
\end{frame}
%% SLIDE 11:
\begin{frame}
\frametitle{Fine Tuning}
	%-----------------------------------------%
	\onslide <1->{
	GoogLeNet performs very well on BelgiumTSC, providing us with a solid baseline on clean data.}
	\onslide <2->{
	\\ ~ \\By performing fine tuning from a pretrained model with ImageNet weights, 20 epochs of training are sufficient to reach $98.4\%$ accuracy on test data.
	}
	
	%-----------------------------------------%
\end{frame}

%% SLIDE 12:
\begin{frame}
\frametitle{FGSM Attack}
%-----------------------------------------%
\only<1>{For $\epsilon = [0.01, 0.05, 0.1, 0.15, 0.2, 0.3] $ 
	\begin{figure}
		\includegraphics[scale=0.4]{images/fgsm_attack_before.png}
\end{figure}}

\only<2>{
	\begin{figure}
		\centering
		\includegraphics[scale=0.26]{images/FGSM_before_train_good.png}
	\end{figure}
}

%-----------------------------------------%
\end{frame}


%% SLIDE 13:
\begin{frame}
\frametitle{FGSM Defense}
%-----------------------------------------%
\only<1>{For each $\epsilon$, we re-trained the network by considering the adversarial examples 
\begin{figure}
	
		\begin{table}
			\begin{tabular}{l | c | c}
				$\epsilon$ & Clean Accuracy & Adversarial Accuracy  \\
				\hline \hline
				 0.01 & 98.30\% & 96.94\% \\ 
				 0.05 & 96.81\% & 92.74\% \\
				 0.1 & 95.02\% & 90.91\% \\
				 0.15 & 96.39\% & 94.21\% \\
				 0.2 & 95.36\% & 92.50\% \\
				 0.3 & 95.52\% & 92.58\% \\
			\end{tabular}
			
		\end{table}
\end{figure}}

\only<2>{\begin{figure}
		\includegraphics[scale=0.4]{images/fsgm_accuracy.png}
\end{figure}}

\only<3>{
\begin{figure}
	\centering
	\includegraphics[scale=0.26]{images/FGSM_after_train_good.png}
\end{figure}
}
\only<4>{\begin{figure}
	\includegraphics[scale=0.4]{images/new_fgsm.png}
	
\end{figure}}



\end{frame}

%% SLIDE 12:
\begin{frame}
\frametitle{PGD Attack}
%-----------------------------------------%
\only<1>{For $\epsilon = [0.01, 0.05, 0.1, 0.15, 0.2, 0.3] $ 
	\begin{figure}
		\includegraphics[scale=0.4]{images/pgd_attack_before.png}
\end{figure}}

\only<2>{\begin{figure}
		\includegraphics[scale=0.26]{images/before_pgd.png}
\end{figure}}



%-----------------------------------------%
\end{frame}


\begin{frame}
\frametitle{PGD Defense}
%-----------------------------------------%
\only<1>{Also in this case, we re-trained the network by considering the adversarial examples 
	\begin{figure}
		
		\begin{table}
			\begin{tabular}{l | c | c}
				$\epsilon$ & Clean Accuracy & Adversarial Accuracy  \\
				\hline \hline
				0.01 & 97.46\% & 94.52\% \\ 
				0.05 & 96.83\% & 86.07\% \\
				0.1 & 97.74\% & 82.66\% \\
				0.15 & 98.02\% & 77.26\% \\
				0.2 & 97.26\% & 69.80\% \\
				0.3 & 96.27\% & 64.52\% \\
			\end{tabular}
			
		\end{table}
\end{figure}}

\only<2>{\begin{figure}
		\includegraphics[scale=0.4]{images/plot_accuracies_PGD.png}
\end{figure}}

\only<3>{
	\begin{figure}
		\centering
		\includegraphics[scale=0.26]{images/PGD_after_train.png}
	\end{figure}}
\only<4>{\begin{figure}
	\includegraphics[scale=0.4]{images/new_pgd.png}
	
\end{figure}}
\end{frame}


\begin{frame}[t]
\frametitle{Single Pixel Attack}
%-----------------------------------------%

\only<1>{\begin{figure}
		\includegraphics[scale=0.6]{images/accuracy_onepixel.png}
\end{figure}}

\only<2>{\begin{figure}
		\includegraphics[scale=0.23]{images/onepixel.png}
		\includegraphics[scale=0.23]{images/maxspeed2.png}
		\includegraphics[scale=0.23]{images/turn.png}
\end{figure}}



%-----------------------------------------%
\end{frame}
\begin{frame}[t]
\frametitle{Smoothing defense: median filter}
\only<1>{\begin{figure}
		\includegraphics[scale=0.5]{images/median.png}
		\end{figure}
		
		\begin{figure}
		\includegraphics[scale=0.5]{images/filtered.jpeg}
		
\end{figure}}
\only<2>{\begin{figure}
		\includegraphics[scale=0.5]{images/attack_smoothing.png}
\end{figure}
This defense technique produces an accuracy of around 97\% for any number of modified pixels between 1 and 5.

}

%-----------------------------------------%
\end{frame}
\section{Conclusions}


 

\begin{frame}[t]

\frametitle{Conclusions}

%-----------------------------------------%


 

\begin{itemize}

\item Black-box attacks are more realistic than white-box ones but have lower success rates.

\item Adversarial training is efficient in increasing accuracy on adversarial data but causes clean performance degradation (robustness/accuracy trade-off).

\item Smoothing defense works very well against Single Pixel attack but problems may arise if many neighboring pixels are attacked simultaneously.

\item Many more black-box attacks are possible and the defense against them is an active field of research.

\end{itemize}

%-----------------------------------------%

\end{frame}

\begin{frame}

\vfil
\hfil \textcolor{mygreen}{\Huge Thank you for your attention!} \hfil
\vfil

\end{frame}


\end{document}